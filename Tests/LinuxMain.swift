import XCTest
import SelfHostedVideoTests

fatalError("Running tests like this is unsupported. Run the tests again by using `swift test --enable-test-discovery`")
XCTMain()
