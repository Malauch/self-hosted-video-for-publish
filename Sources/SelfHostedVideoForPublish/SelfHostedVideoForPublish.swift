import Publish
import Ink
import Plot
import Foundation

public extension Plugin {
	static func selfHostedVideo() -> Self {
		Plugin(name: "ReplaceImageUrl") { context in
			context.markdownParser.addModifier(.selfHostedVideo())
		}
	}
}

public extension Modifier {
	static func selfHostedVideo() -> Self {
		/// Function matches regex in provided string. It is curried function, which first argument is string and second one is regex to match.
		/// - Parameter string: String for matching with regex.
		/// - Returns: Function which accepts regex as String and returned optional matched string.
		func matchRegexIn(string: String) -> (String) -> String? {
			return { regex in
				guard let range = string.range(of: regex, options: [.regularExpression]) else { return nil }
				return String(string[range])
			}
		}
		
		return Modifier(target: .images) { (html, markdown) -> String in
			let matchRegexInHtml = matchRegexIn(string: html)
			
			let videoPathRegex = #"(?<=src=")(.+?)(?=.mp4")"#
			guard matchRegexInHtml(videoPathRegex) != nil else { return html }
			
			let allAttributesPattern = #"(?<=<img\s)([\w\W]+?)["\w](?=\s*/>)"#
			let allAttributes = matchRegexInHtml(allAttributesPattern) ?? ""
			let attrValuePattern = #"(?<=\=")(.+?)(?=")"#
			
			let stringRange = NSRange(allAttributes.startIndex..<allAttributes.endIndex, in: allAttributes)
			guard let regex = try? NSRegularExpression(pattern: attrValuePattern, options: []) else {
				return html
			}
			
			let spToken = "#$%sp%$#"
			var attributesDict = regex
				.matches(in: allAttributes, options: [], range: stringRange)
				.map { Range($0.range, in: allAttributes) }
				.reduce(allAttributes) { result, range in
					guard let range = range else { return result }
					
					let originalValue = allAttributes[range]
					let valueWithoutSpace = originalValue.replacingOccurrences(of: " ", with: spToken)
					return result.replacingOccurrences(of: originalValue, with: valueWithoutSpace)
				}
				.split(separator: " ")
				.map { $0.replacingOccurrences(of: spToken, with: " ") }
				.reduce(into: [String: String]()) { (result, attribute) in
					guard let equalIdx = attribute.firstIndex(of: "=") else {
						result[attribute] = "true"
						return
					}
					
					let attributeName = String(attribute[attribute.startIndex..<equalIdx])
					let value = attribute[attribute.index(after: equalIdx)..<attribute.endIndex]
						.trimmingCharacters(in: ["\""])
					
					result[attributeName] = value
				}
			
			guard let src = attributesDict.removeValue(forKey: "src") else { return html }
			let alt = attributesDict.removeValue(forKey: "alt")
			
			let attributesSorted = attributesDict.sorted { $0.key < $1.key }
			
			let videoHtml = Node.video(
				.forEach(attributesSorted) { (name, value) -> Node<HTML.VideoContext> in
					.attribute(named: name, value: value)
				},
				.source(
					.src(src),
					.type(.mp4)
				),
				.text(alt ?? "Sorry, your browser doesn't support embedded videos.")
			)
						
			let imgRegex = #"<img([\w\W]+?)[\w"\s]/>"#
			let resultHtml = html.replacingOccurrences(of: imgRegex, with: videoHtml.render(), options: [.regularExpression])
			
			return resultHtml
		}
	}
}
