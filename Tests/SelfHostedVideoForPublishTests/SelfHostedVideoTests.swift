import XCTest
@testable import SelfHostedVideoForPublish
import Ink

final class SelfHostedVideoTests: XCTestCase {
	func test_mdWithAltTitleMp4() {
		// given
		let md = #"![Alternate text](url.mp4 "Title")"#
		let parser = MarkdownParser(modifiers: [.selfHostedVideo()])
		let expectedOutput = #"<video title="Title"><source src="url.mp4" type="video/mp4"/>Alternate text</video>"#
		// when
		let html = parser.parse(md).html

		// then
		XCTAssertEqual(expectedOutput, html)
	}
	
	func test_mdWithAltTitleMp4Figure() {
		// given
		let md = #"!![Alternate text](url.mp4 "Title")"#
		let parser = MarkdownParser(modifiers: [.selfHostedVideo()])
		let expectedOutput = #"""
		<figure>
			<video title="Title"><source src="url.mp4" type="video/mp4"/>Alternate text</video>
			<figcaption>Title</figcaption>
		</figure>
		"""#

		// when
		let html = parser.parse(md).html
		
		// then
		XCTAssertEqual(expectedOutput, html, MarkdownParser().parse(md).html)
	}
	
	func test_mdWithAltTitleMp4AndAdditionalAttributes() {
		// given
		let md = #"![Alternate text](url.mp4 "Title" width="100" height="100" controls)"#
		let parser = MarkdownParser(modifiers: [.selfHostedVideo()])
		let expectedOutput = #"<video controls="true" height="100" title="Title" width="100"><source src="url.mp4" type="video/mp4"/>Alternate text</video>"#
		// when
		let html = parser.parse(md).html
		
		// then
		XCTAssertEqual(expectedOutput, html)
	}
	
	func test_mdWithOnlyMp4() {
		// given
		let md = #"![](url.mp4)"#
		let parser = MarkdownParser(modifiers: [.selfHostedVideo()])
		let expectedOutput = #"<video><source src="url.mp4" type="video/mp4"/>Sorry, your browser doesn't support embedded videos.</video>"#
		// when
		let html = parser.parse(md).html
		
		// then
		XCTAssertEqual(expectedOutput, html)
	}
	
	func test_image() {
		// given
		let md = #"![](url.png)"#
		let parser = MarkdownParser(modifiers: [.selfHostedVideo()])
		let expectedOutput = #"<img src="url.png"/>"#
		// when
		let html = parser.parse(md).html
		
		// then
		XCTAssertEqual(expectedOutput, html)
	}
}
