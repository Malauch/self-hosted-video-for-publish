// swift-tools-version:5.3

import PackageDescription

let package = Package(
	name: "SelfHostedVideoForPublish",
	products: [
		.library(
			name: "SelfHostedVideoForPublish",
			targets: ["SelfHostedVideoForPublish"]),
	],
	dependencies: [
		.package(name: "Publish", url: "https://github.com/johnsundell/publish.git", from: "0.7.0"),
		.package(name: "Ink", url: "https://gitlab.com/Malauch/ink.git", .branch("figureSupport"))
	],
	targets: [
		.target(
			name: "SelfHostedVideoForPublish",
			dependencies: ["Publish", "Ink"]),
		.testTarget(
			name: "SelfHostedVideoForPublishTests",
			dependencies: ["SelfHostedVideoForPublish"]),
	]
)
